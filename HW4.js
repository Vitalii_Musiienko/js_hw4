let userNumber1;
let userNumber2;
let operator;

function isNumberValid(a) {
    if (typeof +a === "number" && !Number.isNaN(+a) && a.length > 0 && a[0] !== " ") {
        return false;
    } else {
        return true;
    }
}

while (isNumberValid(userNumber1)) {
    userNumber1 = prompt("Enter first number", userNumber1);
}

while (isNumberValid(userNumber2)) {
    userNumber2 = prompt("Enter second number", userNumber2);
}

function isOperatorValid(x) {
    switch (x) {
        case "+":
            break;
        case "-":
            break;
        case "*":
            break;
        case "/":
            break;
        default:
            return false;
    }
    return true;
}

while (!isOperatorValid(operator)) {
    operator = prompt("Enter an operator for number (+, -, *, /)");
}

function calc(a, b, c) {
    if (c === "+") {
        return +a + +b;
    }
    if (c === "-") {
        return a - b;
    }
    if (c === "*") {
        return a * b;
    }
    if (c === "/") {
        return a / b;
    }
}

console.log(calc(userNumber1, userNumber2, operator));